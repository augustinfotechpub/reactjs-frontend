import React, { useState, useEffect } from 'react';
import LeftPanel from "../components/LeftPanel";
import HeaderMobile from "../components/HeaderMobile";
import DashboardTopSection from "../components/dashboard/DashboardTopSection";
import DashboardMiddleSection from "../components/dashboard/DashboardMiddleSection";
import DashboardTableSection from "../components/dashboard/DashboardTableSection";
import { Row, Col } from 'react-bootstrap';
import axios from 'axios';
import { ENV } from '../env/env'
import RenewToken from '../components/inner-components/RenewToken';


//agora chat and calllogin
import { useGlobalState, useGlobalMutation } from './utils/container'
import { log } from './utils/utils'
import useRouter from './utils/use-router'
import { Outlet, useNavigate } from 'react-router-dom'
import { Routes, Route } from 'react-router-dom'


const Dashboard = (props) => {
  const [facilities, setFacilities] = useState([])
  const [profileImage,setProfileImage]=useState()
  const [url, setUrl] = useState('')
  const [userData, setUserData] = useState({
    address: "",
    avatar_image: "",
    contact_no: "",
    date_of_birth: "",
    email: "",
    firstname: "",
    id: "",
    lastname: "",
    password: "",
    position: "",
    username: "",
    fullname: ""

  })



  useEffect(() => {
    let token = localStorage.getItem('token')
    axios.get(ENV.ENDPOINT + '/user/profile/', { headers: { "Authorization": `Bearer ${token}` } })
      .then(response => {
        setUserData(response.data.msg)
        localStorage.setItem('username1', response.data.msg.username)

        localStorage.setItem('position', response.data.msg.position)
        var link
        if (response.data.msg.Admin != undefined) {
          link = ENV.ENDPOINT + `/admin/view/?admin_id=${response.data.msg.Admin}`
        }

        else if (response.data.msg.user_id != undefined && response.data.msg.position == 'clinician') {
          link = ENV.ENDPOINT + `/clinician/view/?search=${response.data.msg.user_id}`
        }
        axios.get(link, { headers: { "Authorization": `Bearer ${token}` } })
          .then(response => {

            localStorage.setItem('avatarImage', response.data.results[0].avatar_image)
            setProfileImage(response.data.results[0].avatar_image)
            setUrl(2)

            localStorage.setItem('scheduling', response.data.results[0].allow_scheduling)
            if(localStorage.getItem('position')=="facility_admin"||localStorage.getItem('position')=="poc_admin"){
              localStorage.setItem("facility_id", response.data.results[0].facility)
            }
            
          })
          .catch((error) => {
            console.log(error)
          })
      })
      .catch((error) => {
        console.log(error)
        if (error.response.data.code == 'token_not_valid') {
          RenewToken.RenewTokenAPI()
        }

      })

   
    axios.get(ENV.ENDPOINT + '/facility/', { headers: { "Authorization": `Bearer ${token}` } })
      .then(response => {
        setFacilities(response.data.payload)
        if(!localStorage.getItem('facility')&&localStorage.getItem('position')=="poc_admin"||localStorage.getItem('position')=="facility_admin"){
          response.data.payload.map(function(facility){
            if (facility.facility_id==localStorage.getItem("facility_id")){
              localStorage.setItem("facility", facility.facility_name)
            }
          })
      }
        else if(!localStorage.getItem('facility')){
          localStorage.setItem("facility", response.data.payload[0].facility_name)
          localStorage.setItem("facility_id", response.data.payload[0].facility_id)
          
        }
      })
      .catch((error) => {
        console.log(error)
      })



  }, [url])


  const [refresh, setRefresh] = useState(false)
  const handleRefresh = (e) => {
    setRefresh(e)
  }


  //for agora chat and call login
  const navigate = useNavigate()
  const stateCtx = useGlobalState()
  const mutationCtx = useGlobalMutation()

  const routerCtx = useRouter()

  const history = useNavigate()

  // stateCtx.rtmClient.inquire(['3']).then((res) => {
  //   console.log("responseeeeeeeeee",res)
  // })
  // .catch((err)=>{
  //   console.log(err,"response error")
  // })
  useEffect(() => {
    if (stateCtx.loading === true) {
      mutationCtx.stopLoading()
    }
  }, [stateCtx.loading, mutationCtx])

  useEffect(() => {
  //   if (stateCtx.rtmClient.status === 'offLine') {
  //     stateCtx.rtmClient.login(stateCtx.userCode).then(() => {

  //     }).catch(() => {
  //         log('Login failure from dashboard')
  //         // mutationCtx.toastError('Logon failure')
  //         return
  //     })
  // }
    setTimeout(() => {
      
        stateCtx.rtmClient.login(localStorage.getItem('usercode')).then(() => {
  
        })
    
    }, 1000);
  }, [stateCtx.rtmClient])


  useEffect(() => {


    stateCtx.rtmClient.on('RemoteInvitationReceived', (remoteInvitation) => {
      let content=JSON.parse(remoteInvitation.content)
     
      mutationCtx.updatePeerCode(remoteInvitation.callerId)
      mutationCtx.updateIncomingCode(content.name)
      mutationCtx.updateIncomingImage(content.image)
      mutationCtx.updateConfig({ channelName: content.channel })

      navigate({
        pathname: `/call-invi/card/indexCard/incoming/`
      })
    })

    stateCtx.rtmClient.on('RemoteInvitationCanceled', () => {
      console.log('remote invitation cancelled')
      // navigate('/dashboard')
    })

    stateCtx.rtmClient.on('RemoteInvitationFailure', () => {
      mutationCtx.toastError('Call not answered')
      navigate.goBack()
    })

    stateCtx.rtmClient.on('RemoteInvitationAccepted', () => {
      log('Accept success')
      mutationCtx.startLoading()
      let channel = stateCtx.config.channelName
      log('channel id', channel)
      navigate({
        pathname: `/meeting/${channel}`
      })
    })

    stateCtx.rtmClient.on('LocalInvitationAccepted', () => {
      mutationCtx.startLoading()
      let channel = stateCtx.config.channelName
      log('channel id', channel)
      navigate({
        pathname: `/meeting/${channel}`
      })
    })

    stateCtx.rtmClient.on('LocalInvitationRefused', () => {
      mutationCtx.toastError('Peer is busy')
      navigate.goBack()
    })

    stateCtx.rtmClient.on('RemoteInvitationRefused', () => {
      console.log("call declined from inter-coding-card")
      navigate('/call-invi')
    })

    stateCtx.rtmClient.on('LocalInvitationCanceled', () => {
      navigate.goBack()
    })

    stateCtx.rtmClient.on('LocalInvitationReceivedByPeer', () => {
      navigate({
        pathname: `/dialling`
      })
    })

    stateCtx.rtmClient.on('LocalInvitationFailure', () => {
      mutationCtx.toastError('Call process failed')
      history.goBack()
    })

    return () => {
      stateCtx.rtmClient.eventBus.removeAllListeners()
      //   navigate('/card')
    }

  }, [stateCtx.rtmClient, stateCtx])

  return (
    <>
      <main className='app-main'>
      {localStorage.getItem('scheduling')?<HeaderMobile />:""}
        
        <section className="main-section dashboard-section blue-bg-section">
          <div className="section-wrap">
            <Row className="dashboard-row-main">
              {localStorage.getItem('scheduling')?<LeftPanel profileImage={profileImage}/>:""}
              <Col lg="9" className="right-content-col grey-bg-col">
                <DashboardTopSection facilities={facilities} handleRefresh={handleRefresh} />
                <DashboardMiddleSection handleRefresh={handleRefresh} refresh={refresh} />
                <DashboardTableSection handleRefresh={handleRefresh} refresh={refresh} />
              </Col>
            </Row>
          </div>
        </section>
      </main>
    </>
  )
}

export default Dashboard;