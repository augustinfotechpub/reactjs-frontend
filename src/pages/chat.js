// import React, { useEffect, useRef } from "react";
// import AgoraRTM from "agora-rtm-sdk";
// import { v4 as uuidv4 } from 'uuid';
// import { useState } from "react";

// const APP_ID = '';
// const client = AgoraRTM.createInstance(APP_ID);
// const uid = uuidv4()
// const CHANNEL_NAME = '12345'

// const Chat = () => {
//     const [text, setText] = useState('')
//     const [channel, setChannel] = useState()
//     const [messages, setMessages] = useState([])
//     const messagesRef= useRef();
//     useEffect(() => {
//         const connect = async () => {
//             await client.login({ uid, token: null})
//             const channel = await client.createChannel(
//                 CHANNEL_NAME
//             );
//             await channel.join();
//             channel.on('ChannelMessage', (message, peerId) => {
//                 setMessages(currentMessages => [...currentMessages, {
//                     uid: peerId,
//                     text: message.text
//                 }])
//             })
//             setChannel(channel);
//             return channel;
//         }
//         const connection = connect();

//         return () => {
//             const logout = async () => {
//                 const channel=await connection
//                 await channel.leave();
//                 await client.logout();
//             }
//             logout();
//         }
//     }, [])

//     useEffect(()=>{
//         messagesRef.current.scrollTop=messagesRef.current.scrollHeight;
//     },[messages])

//     const sendMessage = (e) => {
//         e.preventDefault()
//         if (text == '') return;
//         channel.sendMessage({
//             text,
//             type: 'text',
//         })
//         setMessages(currentMessages => [...currentMessages, {
//             uid,
//             text,
//         }])
//         setText('')

//     }
//     return (


//        <main>
//             <div className="panel">
//                 <div className="messages" ref={messagesRef}>
//                     <div className="inner">
//                     {messages.map((message, index) => (
//                         <div key={index} className="message">
//                             {message.uid === uid && (
//                                 <div className="user-self">You:&nbsp;</div>
//                             )}
//                             {message.uid !== uid && (
//                                 <div className="user-self">Them:&nbsp;</div>   
//                             )}
//                             <div className="text">{message.text}</div> 
//                         </div>
//                     ))}
//                     </div>
//                 </div>
//                 <form onSubmit={sendMessage}>
//                     <input value={text} onChange={(e) => setText(e.target.value)}></input>
//                     <button>Send</button>
//                 </form>
//             </div>
//        </main>
            
//     )
// }

// export default Chat;