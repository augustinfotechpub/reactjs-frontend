// import React, { useState, useEffect } from 'react';

// import AgoraRTC from "agora-rtc-sdk";

// const Call = (props) => {
//     // Handle errors.
//     let handleError = function (err) {
//         console.log("Error: ", err);
//     };

//     // Query the container to which the remote stream belong.
//     let remoteContainer = document.getElementById("remote-container");

//     // Add video streams to the container.
//     function addVideoStream(elementId) {
//         // Creates a new div for every stream
//         let streamDiv = document.createElement("div");
//         // Assigns the elementId to the div.
//         streamDiv.id = elementId;
//         // Takes care of the lateral inversion
//         streamDiv.style.transform = "rotateY(180deg)";
//         // Adds the div to the container.
//         remoteContainer.appendChild(streamDiv);
//     };

//     // Remove the video stream from the container.
//     function removeVideoStream(elementId) {
//         let remoteDiv = document.getElementById(elementId);
//         if (remoteDiv) remoteDiv.parentNode.removeChild(remoteDiv);
//     };









//     let client = AgoraRTC.createClient({
//         mode: "rtc",
//         codec: "vp8",
//     });

//     client.init("a7f253fadcd1445babfad0bf59f3c1c2", function () {
//         console.log("client initialized");
//     }, function (err) {
//         console.log("client init failed ", err);
//     });

//     client.join("007eJxTYOCRPt/puIT/xeHzB9gTd74/w+DDdn1SS3TDupOREWkFNucVGBLN04xMjdMSU5JTDE1MTJMSk4Bsg6Q0U8s042TDZKO7zabJMVfNkncHtTAxMkAgiM/KYGhkbGLKwAAAxMIh5A==", "12345", null, (uid) => {
//         // Create a local stream

//         let localStream = AgoraRTC.createStream({
//             audio: true,
//             video: false,
//         });
//         // Initialize the local stream
//         localStream.init(() => {
//             // Play the local stream
//             localStream.play("me");
//             // Publish the local stream
//             client.publish(localStream, handleError);
//         }, handleError);

//     }, handleError);

//     // Subscribe to the remote stream when it is published
//     client.on("stream-added", function (evt) {
//         client.subscribe(evt.stream, handleError);
//     });
//     // Play the remote stream when it is subscribed
//     client.on("stream-subscribed", function (evt) {
//         let stream = evt.stream;
//         let streamId = String(stream.getId());
//         addVideoStream(streamId);
//         stream.play(streamId);
//     });

//     // Remove the corresponding view when a remote user unpublishes.
//     client.on("stream-removed", function (evt) {
//         let stream = evt.stream;
//         let streamId = String(stream.getId());
//         stream.close();
//         removeVideoStream(streamId);
//     });
//     // Remove the corresponding view when a remote user leaves the channel.
//     client.on("peer-leave", function (evt) {
//         let stream = evt.stream;
//         let streamId = String(stream.getId());
//         stream.close();
//         removeVideoStream(streamId);
//     });

//     return (
//         <>
//             <h1>Hello</h1>
//         </>
//     )
// }

// export default Call;






// import React, { Component } from "react";
// import AgoraRTC from "agora-rtc-sdk";


// let client = AgoraRTC.createClient({ mode: "rtc", codec: "vp8" });
// // client.setClientRole()

// const USER_ID = 12345;
// const APP_ID = "fc5136ca373f47599de2ef68059b1663";

// let data_ = {
//   streamID: USER_ID,
//   audio: true,
//   video: false,
 
// }

// // if (isFirefox()) {
// //   streamSpec.mediaSource = 'window';
// // } else if (!isCompatibleChrome()) {
// //   streamSpec.extensionId = 'minllpmhdgpndnkomcoccfekfegnlikg';
// // }
// export default class Call extends Component {
//   localStream = AgoraRTC.createStream({
//     ...data_
//   });

//   // for screen share
// //   localStreamScreenShare = AgoraRTC.createStream({
// //     streamID: USER_ID,
// //     audio: false,
// //     video: true,
// //     screen: false,
// //   });

//   state = {
//     remoteStreams: []
//   };

//   componentDidMount() {
//     this.initLocalStream();
//     this.initClient();
//   }

//   componentDidUpdate(prevProps, prevState) {
//     if (prevProps.channel !== this.props.channel && this.props.channel !== "") {
//       this.joinChannel();
//     }
//   }

//   initLocalStream = () => {
//     let me = this;
//     me.localStream.init(
//       function () {
//         console.log("getUserMedia successfully");
//         me.localStream.play("agora_local");
//       },
//       function (err) {
//         console.log("getUserMedia failed", err);
//       }
//     );
//     // me.localStreamScreenShare.init(
//     //   function () {
//     //     console.log("getUserMedia successfully");
//     //     me.localStreamScreenShare.play("agora_local-screenshare");
//     //   },
//     //   function (err) {
//     //     console.log("getUserMedia failed", err);
//     //   }
//     // );
//   };

//   initClient = () => {
//     client.init(
//       APP_ID,
//       function () {
//         console.log("AgoraRTC client initialized");
//       },
//       function (err) {
//         console.log("AgoraRTC client init failed", err);
//       }
//     );
//     this.subscribeToClient();
//   };

//   subscribeToClient = () => {
//     let me = this;
//     client.on("stream-added", me.onStreamAdded);
//     client.on("stream-subscribed", me.onRemoteClientAdded);

//     client.on("stream-removed", me.onStreamRemoved);

//     client.on("peer-leave", me.onPeerLeave);
//   };

//   onStreamAdded = evt => {
//     let me = this;
//     let stream = evt.stream;
//     console.log("New stream added: " + stream.getId());
//     me.setState(
//       {
//         remoteStreams: {
//           ...me.state.remoteStream,
//           [stream.getId()]: stream
//         }
//       },
//       () => {
//         // Subscribe after new remoteStreams state set to make sure
//         // new stream dom el has been rendered for agora.io sdk to pick up
//         client.subscribe(stream, function (err) {
//           console.log("Subscribe stream failed", err);
//         });
//       }
//     );
//   };

//   joinChannel = () => {
//     let me = this;
//     client.join(
//       null,
//       me.props.channel,
//       USER_ID,
//       function (uid) {
//         console.log("User " + uid + " join channel successfully");
//         client.publish(me.localStream, function (err) {
//           console.log("Publish local stream error: " + err);
//         });
//         // client.publish(me.localStreamScreenShare, function (err) {
//         //   console.log("Publish local stream error: " + err);
//         // });

//         client.on("stream-published", function (evt) {
//           console.log("Publish local stream successfully");
//         });
//       },
//       function (err) {
//         console.log("Join channel failed", err);
//       }
//     );
//   };

//   onRemoteClientAdded = evt => {
//     let me = this;
//     let remoteStream = evt.stream;
//     me.state.remoteStreams[remoteStream.getId()].play(
//       "agora_remote " + remoteStream.getId(),
//       "agora_remote-screenstreem " + remoteStream.getId()
//     );
//   };

//   onStreamRemoved = evt => {
//     let me = this;
//     let stream = evt.stream;
//     if (stream) {
//       let streamId = stream.getId();
//       let { remoteStreams } = me.state;

//       stream.stop();
//       delete remoteStreams[streamId];

//       me.setState({ remoteStreams });

//       console.log("Remote stream is removed " + stream.getId());
//     }
//   };

//   onPeerLeave = evt => {
//     let me = this;
//     let stream = evt.stream;
//     if (stream) {
//       let streamId = stream.getId();
//       let { remoteStreams } = me.state;

//       stream.stop();
//       delete remoteStreams[streamId];

//       me.setState({ remoteStreams });

//       console.log(evt.uid + " leaved from this channel");
//     }
//   };

//   render() {
//     return (
//       <div className="streeming-main">
//         <div id="agora_local" style={{ width: "400px", height: "400px" }} />
//         {Object.keys(this.state.remoteStreams).map(key => {
//           let stream = this.state.remoteStreams[key];
//           let streamId = stream.getId();
//           return (
//             <div
//               key={streamId}
//               id={`agora_remote ${streamId}`}
//               style={{ width: "400px", height: "400px" }}
//             />
//           );
//         })}
//         <div id="agora_local-screenshare" style={{ width: "400px", height: "400px" }} />
//         {Object.keys(this.state.remoteStreams).map(key => {
//           let stream = this.state.remoteStreams[key];
//           let streamId = stream.getId();
//           return (
//             <div
//               key={streamId}
//               id={`agora_remote-screenstreem ${streamId}`}
//               style={{ width: "400px", height: "400px" }}
//             />
//           );
//         })}
//       </div>
//     );
//   }
// }