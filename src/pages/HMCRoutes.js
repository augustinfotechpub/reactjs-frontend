import React, { useEffect, useState } from 'react';
import { Navigate,Outlet } from 'react-router-dom';
import Auth from './Auth';
function HMCRoute() {

   let auth=false

   useEffect(()=>{
    if(localStorage.getItem("position")=="hmc_admin"){
        auth=true
    }
   })
    return (

            auth
            ? <Outlet/>
            : <Navigate to={'/notFound'}   />
    )
}
export default HMCRoute;