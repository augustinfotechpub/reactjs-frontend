import React, { useEffect } from 'react'
import { useGlobalState, useGlobalMutation } from './utils/container'
import { makeStyles } from '@material-ui/core/styles'
import { Container } from '@material-ui/core'
import { log } from './utils/utils'
import useRouter from './utils/use-router'
import { Outlet, useNavigate } from 'react-router-dom'
import { Routes, Route } from 'react-router-dom'

import Card from './index/card'
import Meeting from './meeting/index'
import Calling from './calling'
import Incoming from './incoming'


export default {
    AgoraLogin: ()=>{

        const navigate=useNavigate()
        const stateCtx = useGlobalState()
        const mutationCtx = useGlobalMutation()
        
        const routerCtx = useRouter()
      
        const history = useNavigate()
          console.log(stateCtx)
        useEffect(() => {
          if (stateCtx.loading === true) {
            mutationCtx.stopLoading()
          }
        }, [stateCtx.loading, mutationCtx])
      
        useEffect(() => {
          if (stateCtx.rtmClient.status === 'offLine') {
            stateCtx.rtmClient.login(stateCtx.userCode).then(() => {
              log( stateCtx.userCode ,'is online')
            }).catch(() => {
              log('Login failure')
              mutationCtx.toastError('Logon failure')
              return
            })
          }
        }, [stateCtx.rtmClient])
        
        useEffect(() => {
         
          
          stateCtx.rtmClient.on('RemoteInvitationReceived', (remoteInvitation) => {
           
            mutationCtx.updatePeerCode(remoteInvitation.callerId)
            mutationCtx.updateIncomingCode(remoteInvitation.callerId)
            mutationCtx.updateConfig({ channelName: remoteInvitation.content })
            
           navigate({
              pathname: `/call-invi/card/indexCard/incoming/`
            })
          })
      
          stateCtx.rtmClient.on('RemoteInvitationCanceled', () => {
            navigate.goBack()
          })
      
          stateCtx.rtmClient.on('RemoteInvitationFailure', () => {
            mutationCtx.toastError('Call not answered')
            navigate.goBack()
          })
      
          stateCtx.rtmClient.on('RemoteInvitationAccepted', () => {
            log('Accept success')
            mutationCtx.startLoading()
            let channel = stateCtx.config.channelName
            log('channel id', channel)
            navigate({
              pathname: `/meeting/${channel}`
            }) 
          })
      
          stateCtx.rtmClient.on('LocalInvitationAccepted', () => {
            mutationCtx.startLoading()
            let channel = stateCtx.config.channelName
            log('channel id', channel)
            navigate({
              pathname: `/meeting/${channel}`
            })
          }) 
      
          stateCtx.rtmClient.on('LocalInvitationRefused', () => {
            mutationCtx.toastError('Peer is busy')
            navigate.goBack()
          })
      
          stateCtx.rtmClient.on('RemoteInvitationRefused', () => {
              console.log("call declined from inter-coding-card")
              navigate('/call-invi')
          })
      
          stateCtx.rtmClient.on('LocalInvitationCanceled', () => {
            navigate.goBack()
          })
      
          stateCtx.rtmClient.on('LocalInvitationReceivedByPeer', () => {
            navigate({
              pathname: `/dialling`
            })
          })
      
          stateCtx.rtmClient.on('LocalInvitationFailure', () => {
            mutationCtx.toastError('Call process failed')
            history.goBack()
          })
      
          if(localStorage.getItem('peer_id')!=undefined){
            navigate('/call-invi/card')
          }
          
          return () => {
            stateCtx.rtmClient.eventBus.removeAllListeners()
          //   navigate('/card')
          }
          
        }, [stateCtx.rtmClient, stateCtx])
    }
}
