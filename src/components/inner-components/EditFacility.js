import React, { useState, useEffect } from 'react';
import { Form, Row, Col } from 'react-bootstrap';
import InputGroupForm from './InputGroupForm';
import ImageUploadInput from './ImageUploadInput';
import { useNavigate,Link } from 'react-router-dom';
import { RoutesPath } from '../../App';
import  {useLocation} from 'react-router-dom'
import axios from "axios";
import { ENV } from '../../env/env'
import { Buffer } from 'buffer';
import S3FileUpload from 'react-s3';

const EditFacility = () => {
    window.Buffer = Buffer;
    const navigate = useNavigate();
    const location=useLocation();
   
    const [image, setImage] = useState(location.state.data.facility_id)

    const [model, setModel] = useState({
        userName:location.state.data.username,
        Email:location.state.data.email,
        Password:location.state.data.password,
        pocFirstName:location.state.data.poc_firstname,
        pocLastName:location.state.data.poc_lastname,
        PocPosition:location.state.data.poc_position,
        FacilityName:location.state.data.facility_name,
        Mainline:location.state.data.phone_no,
        Address:location.state.data.address,
        Image:location.state.data.facility_image
    });
    
    async function apiCall(credentials) {
        let token=localStorage.getItem('token')
        
        await axios.patch(ENV.ENDPOINT+`/facility/edit/${location.state.data.facility_id}/`,credentials,{headers:{"Authorization":`Bearer ${token}`}})
        .then(response=>{
            
            console.log(response.data.messages)
            
        })
        .catch((error)=>{
            console.log(error.response.data)
        })
     }


    

    const handleChange = (event) => {
        // get the value of the field
        console.log("hello from child")
        const value = event?.target?.value;
        //set it in the model
        setModel({
          ...model,
          [event.target.name]: value
        });
        console.log(model.userName,"usernameeeeeeeeeeeeeeee")
        console.log(model.Email,"email")

      };
      
    const handleImage = (e) => {
        console.log(e)
        setImage(e)
    }
    const config = {
        bucketName: 'healthshiftalerts-images',
        region: 'us-east-1',
        accessKeyId: process.env.REACT_APP_accessKey,

        secretAccessKey: process.env.REACT_APP_secretKey
    }

    const handleSubmit=(event)=>{
        
        var imageLocation
        S3FileUpload.uploadFile(image, config)

        .then((data)=>{

              console.log(data);// it return the file url
              imageLocation=data.location

            
              let username=model.userName
              let email=model.Email
              let password=model.Password
              let address=model.Address
              let poc_position=model.PocPosition
              let poc_firstname=model.pocFirstName
              let poc_lastname=model.pocLastName
              let phone_no=model.Mainline
              let facilty_image=imageLocation
              let facility_name=model.FacilityName
              
      
          const token =  apiCall({
              username,
              email,
              password,
              address,
              poc_position,
              poc_firstname,
              poc_lastname,
              phone_no,
              facilty_image,
              facility_name
          });

         })

         .catch((err) =>{

               alert(err);

          });

        
       

    navigate('/manage-users')
      }

    const handleDelete=()=>{
        let token=localStorage.getItem('token')
            
            axios.delete(ENV.ENDPOINT+`/facility/delete/${location.state.data.facility_id}/`,{headers:{"Authorization":`Bearer ${token}`}})
            .then(response=>{
                
                // console.log(response.data.messages)
                console.log(response.data.results)
                
                
            })
            .catch((error)=>{
                console.log(error.response.data)
            })
            navigate('/manage-users')
    }
    return (
        <div className='edit-user-wrapper'>
            <div className='user-header'>
                <Link to={RoutesPath.manageUser} className='back-btn' id="UserbackBtn">
                    <i className='icon-Back-arrow'></i>
                </Link>
                <h6 className='poc-name-title'>{location.state.data.facility_name}</h6>
                <button className='btn border-btn'>
                    <i className='icon-message-solid'></i>
                    Message
                </button>
            </div>
            <div className='edit-user'>
                <Form>
                    <div className="edit-user-top">
                        <div className='user-image'>
                            <div className='image-upload'>
                                <ImageUploadInput UserImage={location.state.data.facilty_image} handleImage={handleImage}/>
                            </div>
                        </div>
                        <div className="edit-user-form">
                            <Row className="form-row">
                                <Col md={6}>
                                    <InputGroupForm inputId="userName" inputLabel="User name" inputType="text" inputPlaceholder="Text here" inputName="userName" inputValue={location.state.data.username}  handleChange={handleChange}/>
                                </Col>
                                <Col md={6}>
                                    <InputGroupForm inputId="pocFirstName" inputLabel="POC Firstname" inputType="text" inputPlaceholder="Text here" inputName="pocFirstName" inputValue={location.state.data.poc_firstname}  handleChange={handleChange}/>
                                </Col>
                                <Col md={6}>
                                    <InputGroupForm inputId="Email" inputLabel="Email" inputType="email" inputPlaceholder="Text here" inputName="Email" inputValue={location.state.data.email} handleChange={handleChange}/>
                                </Col>
                                <Col md={6}>
                                    <InputGroupForm inputId="pocLastName" inputLabel="POC Lastname" inputType="text" inputPlaceholder="Text here" inputName="pocLastName" inputValue={location.state.data.poc_lastname}  handleChange={handleChange}/>
                                </Col>
                                <Col md={6}>
                                    <InputGroupForm inputId="Password" inputLabel="Password" inputType="password" inputPlaceholder="*****" inputName="Password"  inputValue={location.state.data.password} handleChange={handleChange}/>
                                </Col>
                                <Col md={6}>
                                    <InputGroupForm inputId="PocPosition" inputLabel="POC position" inputType="text" inputPlaceholder="Text here" inputName="PocPosition" inputValue={location.state.data.poc_position}  handleChange={handleChange}/>
                                </Col>
                            </Row>
                        </div>

                    </div>
                    <div className='edit-user-bottom edit-user-form'>
                        <Row className="form-row">
                            <Col md={6}>
                                <InputGroupForm inputId="FacilityName" inputLabel="Facility Name" inputType="text" inputPlaceholder="Text here" inputName="FacilityName" inputValue={location.state.data.facility_name}  handleChange={handleChange}/>
                            </Col>
                            <Col md={6}>
                                <InputGroupForm inputId="Mainline" inputLabel="Mainline" inputType="text" inputPlaceholder="Text here" inputName="Mainline" inputValue={location.state.data.phone_no}  handleChange={handleChange}/>
                            </Col>
                            <Col md={12}>
                                <InputGroupForm inputId="Address" inputLabel="Address" inputType="text" inputPlaceholder="Text here" inputName="Address" inputValue={location.state.data.address}  handleChange={handleChange}/>
                            </Col>
                        </Row>
                    </div>
                    <div className='edit-user-footer'>
                        <div className='btn-wrap'>
                            <button className='btn border-btn' onClick={handleDelete}>Delete Facility</button>
                            <button className='btn btn-primary' onClick={handleSubmit}>Save Changes</button>
                        </div>
                    </div>
                </Form>
            </div>

        </div>
    )
}

export default EditFacility;