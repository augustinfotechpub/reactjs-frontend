import React, { useState, useEffect } from 'react';
import { Form, Row, Col } from 'react-bootstrap';
import InputGroupForm from './InputGroupForm';
import ImageUploadInput from './ImageUploadInput';
import DateofBirthInput from './DateofBirthInput';
import CustomDropdown from './CustomDropdown';
import SwitchToggle from './SwitchToggle';
import axios from "axios";
import { useNavigate,Link } from 'react-router-dom';
import { ENV } from '../../env/env'


const RoleDropdownData = [
    { value: 'Facility Admin' },
    { value: 'HMC Admin'},
    { value: 'Super Admin' }
]

const AdminTabContent = (props) => {
    const [data,setData]=useState({
        address: "",
        avatar_image: "",
        contact_no: "",
        date_of_birth: "",
        email: "",
        fullname: "",
        admin_id: "",
        password: "",
        role: "",
        facility:""
        
    })
    const[userName,setUserName]=useState()
    const [model, setModel] = useState({
        username:"",
        email:"",
        password:"",
        fullname:"",
        contact_no:"",
        address:""
    },[]);
    const [dob,setDob]=useState()
    const [isLoading,setIsLoading]=useState(true)
    const [modeifiedDate,setModifiedDate]=useState()
    const [roles,setRole]=useState()
    const [Scheduling,setScheduling]=useState()
    const [userData,setUserData]=useState()
     useEffect(()=>{
        let token=localStorage.getItem('token')
        let facilityValue=localStorage.getItem("facility")
           
             
             axios.get(ENV.ENDPOINT+`/admin/view/?search=${facilityValue},hmc_admin,true`,{headers:{"Authorization":`Bearer ${token}`}})
            .then(response=>{
                
                // console.log(response.data.messages)
                console.log(response.data.results[0])
                setData(response.data.results[0])
                setUserName(response.data.results[0].username)
                setIsLoading(false)
                setModel(response.data.results[0])
                setRole(response.data.results[0].role)
                const [year, month, day]=response.data.results[0].date_of_birth.split('-');
                const result1=[ month, day,year].join('/');
                setModifiedDate(result1)

                axios.get(ENV.ENDPOINT+`/myuser/?search=${response.data.results[0].email}`,{headers:{"Authorization":`Bearer ${token}`}})
                .then(response=>{  
                   
                   setUserData(response.data.results[0].id)
             
                    if(response.data.results[0].position=="c_level_emp"){
                        setRole("Super Admin")
                    }
                    else if(response.data.results[0].position=="hmc_admin"){
                        setRole("HMC Admin")
                    }
                    if(response.data.results[0].position=="facility_admin"){
                        setRole("Faciliy Admin")
                    }            
                })
                .catch((error)=>{
                    console.log(error.response.data)
                })
            })
            .catch((error)=>{
                console.log(error.response.data)
            })
           
              
            
            
            
        
    },[])
    
    const navigate = useNavigate();
    console.log(modeifiedDate)
   
   

    
    async function apiCall(credentials) {
        console.log(userData)
        let token=localStorage.getItem('token')
        

        

        await axios.patch(ENV.ENDPOINT+`/admin/edit/${data.admin_id}/`,credentials,{headers:{"Authorization":`Bearer ${token}`}})
        .then(response=>{
            console.log(response.data.messages)
        })
        .catch((error)=>{
            console.log(error.response.data)
        })
        await axios.patch(ENV.ENDPOINT+`/userprofile/edit/${userData}/`,credentials,{headers:{"Authorization":`Bearer ${token}`}})
        .then(response=>{
            console.log(response.data.messages) 
        })
        .catch((error)=>{
            console.log(error.response.data)
        })
     }
     

    

    const handleChange = (event) => {
        // get the value of the field
        console.log("hello from child")
        const value = event?.target?.value;
        //set it in the model
        setModel({
          ...model,
          [event.target.name]: value
        });
       
       
    
      };
      
 
        
       
       
      
   
        
    
    
      const [neww,setNeww]=useState()
      const handledDate=date=>{
          let datevalue=date.format()
          const [month,day,year]=datevalue.split('/');
          const result=[year,month,day].join('-');
          setNeww(result)
          console.log(neww)
          }
    
    
   
    // useEffect(()=>{
    //     if(data.role!=undefined&&data.role!=""){
    //         console.log("roleeeeeeeeeeeeee",data.role)
    //         setRole(data.role)
    //     }
    // })
      
      
    const handleJobSites=(selection)=>{
        console.log(selection.value)
            setRole(selection.value)
         
    }
    const handleUsername=(event)=>{
        
            setUserName(event.target.value)
         
    }
    console.log(dob)
    const handleSubmit=(event)=>{
        let date_of_birth
        if(neww){
          
        date_of_birth=neww
        }
        else{
            const [month,day,year]=modeifiedDate.split('/');
        const result=[year,month,day].join('-');
        date_of_birth=result
        }
    let positionRole=""
    if(roles=="Facility Admin"){
        positionRole="facility_admin"
    }
    else if(roles=="Super Admin"){
        positionRole="c_level_emp"
    }
    else if(roles=="HMC Admin"){
        positionRole="hmc_admin"
    }
        console.log(roles,"dhsfsfsffsdfsd")
        console.log(positionRole,"dhsfsfsffsdfsd")
        console.log(model)
       
        
        let username=userName
        let email=model.email
        let password=model.password
        let address=model.address
        let role=positionRole
        let fullname=model.fullname
        let position=positionRole
       
        
    
        let contact_no=model.contact_no
        let avatar_image="static address"
        let allow_scheduling=Scheduling
      
        

    const token =  apiCall({
        username,
        email,
        password,
        address,
        avatar_image,
        role,
        fullname,
        contact_no,
        date_of_birth,
        allow_scheduling,
        position
        
    });

    //navigate('/manage-users')
      }
     
      const handleScheduleSwitch=(e,id)=>{
        console.log(e.target.checked)
       setScheduling(e.target.checked)
        // async function apiCallSChedulling(credentials) {
        //     let token=localStorage.getItem('token')
        //     await axios.patch(`http://127.0.0.1:8000/admin/allowScheduling/${id}/`,credentials,{headers:{"Authorization":`Bearer ${token}`}})
        //     .then(response=>{ 
        //         console.log(response.data.messages) 
        //     })
        //     .catch((error)=>{
        //         console.log(error.response.data)
        //     })
        //  }
        // let allow_scheduling=e.target.checked
        // const token =  apiCallSChedulling({
        //     allow_scheduling
        // });
    }
    
    const handleDisable=(id)=>{
        function apiCallSChedulling(credentials) {
                let token=localStorage.getItem('token')
                axios.patch(ENV.ENDPOINT+`/admin/allowScheduling/${id}/`,credentials,{headers:{"Authorization":`Bearer ${token}`}})
                .then(response=>{ 
                    console.log(response.data.messages) 
                })
                .catch((error)=>{
                    console.log(error.response.data)
                })
             }
            let is_active=false
            const token =  apiCallSChedulling({
                is_active
            });
    }


    return (

        <div className='admin-tab-content tab-content-wrap'>
            <button className='btn border-btn message-text-btn tab-top-btn'>
                <i className='icon-message-solid'></i>
                Message
            </button>

            <div className='edit-user-wrapper'>
                <div className='user-header mobile-user-header'>
                    <h6 className='h5'></h6>
                    <button className='btn border-btn message-text-btn'>
                        <i className='icon-message-solid'></i>
                        Message
                    </button>
                </div>
                {(data!=undefined && modeifiedDate!=undefined && data.role!="" ) ? (
                <div className='edit-user'>
                    
                        <div className="edit-user-top">
                            <div className='user-image'>
                                <div className='image-upload'>
                                    <ImageUploadInput UserImage={"assets/images/super-admin-profile.png"} />
                                </div>
                            </div>
                            <div className="edit-user-form">
                                <div className='user-header'>
                                    <h6 className='h5'>{data.fullname}</h6>
                                    <button className='btn border-btn message-text-btn'>
                                        <i className='icon-message-solid'></i>
                                        Message
                                    </button>
                                </div>
                                <Row className="form-row">
                                    <Col md={6}>
                                        <InputGroupForm inputId="FullName" inputLabel="Full name" inputType="text" inputPlaceholder="Text here" inputName="fullname"  inputValue={data.fullname}  handleChange={handleChange}/>
                                    </Col>
                                    <Col md={6}>
                                        <InputGroupForm inputId="UserName" inputLabel="User name" inputType="text" inputPlaceholder="Text here" inputName="username" inputValue={userName}  handleChange={ handleUsername} />
                                    </Col>
                                    <Col md={6}>
                                        <InputGroupForm inputId="Email" inputLabel="Email" inputType="email" inputPlaceholder="Text here" inputName="email" inputValue={data.email}  handleChange={handleChange}/>
                                    </Col>
                                    <Col md={6}>
                                        <InputGroupForm inputId="Password" inputLabel="Password" inputType="password" inputPlaceholder="*****" inputName="password" inputValue={data.password}  handleChange={handleChange}/>
                                    </Col>

                                </Row>
                            </div>

                        </div>
                        <div className='edit-user-bottom edit-user-form'>
                            <Row className="form-row">
                                <Col md={6}>
                                    <div className='form-group'>
                                        <label className='form-label'>Role</label>
                                        <CustomDropdown optionData={RoleDropdownData} data={roles} handleChange={handleJobSites}/>
                                    </div>
                                </Col>
                                <Col md={6}>
                                    <div className="form-group date-of-birth-input">
                                        <label className='form-label'>Date of birth</label>
                                        <DateofBirthInput inputValue={modeifiedDate} handleChange={handledDate}/>
                                    </div>
                                </Col>
                                <Col md={6}>
                                    <InputGroupForm inputId="PhoneNumber" inputLabel="Phone" inputType="number" inputPlaceholder="Text here" inputName="contact_no" inputValue={data.contact_no}  handleChange={handleChange}/>
                                </Col>
                                <Col md={6}>
                                    <InputGroupForm inputId="Address" inputLabel="Address" inputType="text" inputPlaceholder="Text here" inputName="address"  inputValue={data.address}  handleChange={handleChange}/>
                                </Col>
                                <Col md={6}>
                                    <div className="scheduling-btn">
                                        <h6>Scheduling Access</h6>
                                        <SwitchToggle dataSwitch={data.allow_scheduling}  data={e=>handleScheduleSwitch(e,data.admin_id)}/>
                                    </div>
                                </Col>
                            </Row>
                        </div>

                        <div className='edit-user-footer'>
                            <div className='btn-wrap'>
                                <button className='btn border-btn' onClick={e=>handleDisable(data.admin_id)}>Disable account</button>
                                <button className='btn btn-primary' onClick={handleSubmit}>Save changes</button>
                            </div>
                        </div>
                    
                
                </div>
        ):`No HMC Admin present for ${localStorage.getItem('facility')} facility`}
            </div>

        </div>
    )



    
    
}


export default AdminTabContent;