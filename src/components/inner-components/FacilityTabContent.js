import React, { useState, useEffect } from 'react';
import AddFacilityPopup from './AddFacilityPopup';
import SearchInput from './SearchInput';
import CustomDropdown from './CustomDropdown';
import UserFacilitiesTable from './UserFacilitiesTable';
import TablePagination from './TablePagination';
import axios from "axios";
import { ENV } from '../../env/env'


const UserFacilityFilter = [
    { value: 'All Facilities', label: 'All Facilities' },
    { value: 'Active', label: 'Active' },
    { value: 'Deactive', label: 'Deactive' }
]

const FacilityTabContent = (props) => {
    const [data, setData] = useState([])
    const [searchInput, setSearchInput] = useState()
    const [filterInput, setFilterInput] = useState()
    const [nextPage, setNextPage] = useState(false)
    const [link, setLink] = useState(ENV.ENDPOINT+`/facility/view/`)
    const [nextLink, setNextLink] = useState(false)
    const [prevLink, setPrevLink] = useState(false)
    const [totalPage, setTotalPage] = useState(false)
    const [currentPage, setCurrentPage] = useState()
    const [reRender, setReRender] = useState(false)

    const handleChange = (event) => {
        setSearchInput(event.target.value)
       
    }
    const handleFilter = (selection) => {
        if (selection.value == "Active") {
            setFilterInput("True")
        }
        else if (selection.value == "Deactive") {
            setFilterInput("False")
        }
        else {
            setFilterInput("")
        }


    }

    const handleRender = () => {
        setReRender(true)
        console.log("called render fumctionnnnn")
    }
    const handleLink = () => {

        let token = localStorage.getItem('token')
        // let link=`http://127.0.0.1:8000/api/clinician/view/`
        axios.get(nextLink, { headers: { "Authorization": `Bearer ${token}` } })
            .then(response => {

                console.log("next buttonnnnnnnnnnn")
                console.log(response.data, "facility responseeeeeee")
                setCurrentPage(response.data.page)
                setTotalPage(response.data.total_pages)
                // console.log(response.data.page,"currentt pageeeeeeeeeeeeeee")
                setData(response.data.results)
                if (response.data.links.next != undefined && response.data.links.next != "") {
                    setNextPage(true)
                    setNextLink(response.data.links.next)
                }
                setPrevLink(response.data.links.previous)
            }, [])
            .catch((error) => {
                console.log(error.response.data)
            })
    }

    const handlePrevLink = () => {
        let token = localStorage.getItem('token')
        console.log("prevvv dataaaaaaaaaaaaaaaaaaaa")
        // let link=`http://127.0.0.1:8000/api/clinician/view/`
        axios.get(prevLink, { headers: { "Authorization": `Bearer ${token}` } })
            .then(response => {

                // console.log(response.data.messages)
                console.log(response.data, "facility responseeeeeee")
                setData(response.data.results)
                setCurrentPage(response.data.page)
                setTotalPage(response.data.total_pages)
                // console.log(response.data.page,"currentt pageeeeeeeeeeeeeee")
                if (response.data.links.next != undefined && response.data.links.next != "") {
                    setNextPage(true)
                    setNextLink(response.data.links.next)


                }
                setPrevLink(response.data.links.previous)
            }, [])
            .catch((error) => {
                console.log(error)
            })
    }

   

    useEffect(() => {
        let token = localStorage.getItem('token')

        if (filterInput != "" && filterInput != undefined) {
            if (searchInput != undefined && searchInput != "") {
                axios.get(ENV.ENDPOINT+`/facility/view/?search=${filterInput},${searchInput}`, { headers: { "Authorization": `Bearer ${token}` } })
                    .then(response => {
                        console.log("condition 111111111111111")

                      
                        console.log(response.data)
                        setCurrentPage(response.data.page)
                        setData(response.data.results)
                        setTotalPage(response.data.total_pages)
                        if (response.data.links.next != undefined && response.data.links.next != "") {
                            setNextPage(true)
                            setNextLink(response.data.links.next)

                        }
                        setPrevLink(response.data.links.previous)
                    })
                    .catch((error) => {
                        console.log(error.response.data)
                    })
            }
            else {
                axios.get(ENV.ENDPOINT+`/facility/view/?search=${filterInput}`, { headers: { "Authorization": `Bearer ${token}` } })
                    .then(response => {
                        console.log("condition 222222222222")

                     
                        console.log(response.data)
                        setCurrentPage(response.data.page)
                        setData(response.data.results)
                        setTotalPage(response.data.total_pages)
                        if (response.data.links.next != undefined && response.data.links.next != "") {
                            setNextPage(true)
                            setNextLink(response.data.links.next)

                        }
                        setPrevLink(response.data.links.previous)


                    })
                    .catch((error) => {
                        console.log(error.response.data)
                    })
            }

        }

        else if (searchInput != undefined && searchInput != "") {
            axios.get(ENV.ENDPOINT+`/facility/view/?search=${searchInput}`, { headers: { "Authorization": `Bearer ${token}` } })
                .then(response => {
                    console.log("condition 333333333333333")

                   
                    console.log(response.data)
                    setCurrentPage(response.data.page)
                    setData(response.data.results)
                    setTotalPage(response.data.total_pages)
                    if (response.data.links.next != undefined && response.data.links.next != "") {
                        setNextPage(true)
                        setNextLink(response.data.links.next)

                    }
                    setPrevLink(response.data.links.previous)


                })
                .catch((error) => {
                    console.log(error.response.data)
                })
        } else {
            console.log("defaultttttttt")
            axios.get(link, { headers: { "Authorization": `Bearer ${token}` } })
                .then(response => {

                 
                    setCurrentPage(response.data.page)
                    setData(response.data.results)
                    setTotalPage(response.data.total_pages)
                    if (response.data.links.next != undefined && response.data.links.next != "") {
                        setNextPage(true)
                        setNextLink(response.data.links.next)

                    }
                    setPrevLink(response.data.links.previous)
                })
                .catch((error) => {
                    console.log(error.response.data)
                })

        }
        // let link=`http://127.0.0.1:8000/api/clinician/view/`



        console.log(currentPage, "current page from useEffevt")

        setReRender(false)
    }, [reRender, searchInput, filterInput])



    console.log("current pagweeee", currentPage)




    return (

        <div className='facility-tab-content tab-content-wrap'>
            <div className='filter-row-wrapper'>
                <AddFacilityPopup ModelTitle="Add Facility" handleRender={handleRender} />
                <div className='table-filter-row'>
                    <SearchInput searchInput={handleChange} />
                    <CustomDropdown optionData={UserFacilityFilter} dropClass="without-search" filterInput={handleFilter} />
                </div>
            </div>
            <div className='user-facilty-table-wrapper pagination-table'>
                <div className="table-wrapper table-responsive">
                    <UserFacilitiesTable searchInputKey={searchInput} filterInputKey={filterInput} nextData={data} handleRender={handleRender} renderData={reRender} />
                </div>
                {/* <TablePagination data={nextLink} prevdata={handlePrevLink} currentPage={currentPage} totalPage={totalPage} handleLink={handleLink} /> */}
                <TablePagination data={nextLink} prevdata={handlePrevLink} currentPage={currentPage} totalPage={totalPage} handleLink={handleLink} />
            </div>

        </div>
    )



}


export default FacilityTabContent;