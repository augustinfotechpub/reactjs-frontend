import React, { useState, useEffect } from 'react';
import FacilityTableMenuDropdown from './FacilityTableMenuDropdown';
import SwitchToggle from './SwitchToggle';
import axios from "axios";
import { ENV } from '../../env/env'

const UserFacilitiesTable = (props) => {

    let token=localStorage.getItem('token')

   const [data,setData]=useState([])
   const [searchInput,setSearchInput]=useState("")
   const [filterInput,setFilterInput]=useState("")
    

   const handleScheduleSwitch=(e,id)=>{
    console.log(e.target.checked)
    async function apiCall(credentials) {
        let token=localStorage.getItem('token')
        await axios.patch(ENV.ENDPOINT+`/facility/edit/${id}/`,credentials,{headers:{"Authorization":`Bearer ${token}`}})
        .then(response=>{ 
            console.log(response.data.payload) 
        })
        .catch((error)=>{
            console.log(error.response.data)
        })
     }
    let allow_scheduling=e.target.checked
    const token =  apiCall({
        allow_scheduling
    });
}
    


    useEffect(() => {
       
                // axios.get(`http://127.0.0.1:8000/api/facility/view/?search=`,{headers:{"Authorization":`Bearer ${token}`}})
                // .then(response=>{
                    
                //     // console.log(response.data.messages)
                //     console.log(response.data.results)
                //     setData(response.data.results)
                    
                // })
                // .catch((error)=>{
                //     console.log(error.response.data)
                // })
               
            
                console.log(props.nextData,"next dataaaaaaaaa")
                setData(props.nextData)
            
        
    
      },[props.nextData,props.renderData]);
    
      
      const handleTimesheetSwitch=(e,id)=>{
        console.log(e.target.checked)
        async function apiCall(credentials) {
            let token=localStorage.getItem('token')
            await axios.patch(ENV.ENDPOINT+`/facility/edit/${id}/`,credentials,{headers:{"Authorization":`Bearer ${token}`}})
            .then(response=>{ 
                console.log(response.data.messages) 
            })
            .catch((error)=>{
                console.log(error.response.data)
            })
         }
     
        let timesheet_approval=e.target.checked
        const token =  apiCall({
            timesheet_approval
        });
    }
   

   


    const UserFacilitiesData = [
        {
            FacilityName: "Bristol Village",
            FacilityAddress: "Bristol Village",
            POCFirstName: "Bristol",
            POCLastName: "Village",
            Mainline: "8787823223",
            FacilityEmail: "bristol@ondek.com",
            FacilityStatus: "Active",
            FaclityStatusButtonClassName: "success",
            TimeSheetApprovChecked: "On",
            SchedulingChecked: "Off",

        },
        {
            FacilityName: "Chillicothe Campus",
            FacilityAddress: "Chillicothe Campus",
            POCFirstName: "Chillicothe",
            POCLastName: "Campus",
            Mainline: "639 777777",
            FacilityEmail: "chillicother@ondek.com",
            FacilityStatus: "Active",
            FaclityStatusButtonClassName: "success",
            TimeSheetApprovChecked: "On",
            SchedulingChecked: "On"

        },
        {
            FacilityName: "First Community Village",
            FacilityAddress: "First Community Village",
            POCFirstName: "First Community",
            POCLastName: "Village",
            Mainline: "639 888888",
            FacilityEmail: "firstcommunity@ondek.com",
            FacilityStatus: "Deactive",
            FaclityStatusButtonClassName: "failed",
            TimeSheetApprovChecked: "On",
            SchedulingChecked: "Off"

        },
        {
            FacilityName: "Toledo",
            FacilityAddress: "Bristol Village",
            POCFirstName: "Bristol",
            POCLastName: "Village",
            Mainline: "8787823223",
            FacilityEmail: "bristol@ondek.com",
            FacilityStatus: "Active",
            FaclityStatusButtonClassName: "success",
            TimeSheetApprovChecked: "On",
            SchedulingChecked: "Off"

        },
        {
            FacilityName: "Pembroke Pines",
            FacilityAddress: "Bristol Village",
            POCFirstName: "Bristol",
            POCLastName: "Village",
            Mainline: "8787823223",
            FacilityEmail: "bristol@ondek.com",
            FacilityStatus: "Active",
            FaclityStatusButtonClassName: "success",
            TimeSheetApprovChecked: "On",
            SchedulingChecked: "On"

        },
        {
            FacilityName: "Orange",
            FacilityAddress: "Bristol Village",
            POCFirstName: "Bristol",
            POCLastName: "Village",
            Mainline: "8787823223",
            FacilityEmail: "bristol@ondek.com",
            FacilityStatus: "Active",
            FaclityStatusButtonClassName: "success",
            TimeSheetApprovChecked: "On",
            SchedulingChecked: "On"

        },
    ]
   let FaclityTrueStatusButtonClassName= "success"
   let FaclityFalseStatusButtonClassName= "failed"
   
   
    return (
        
        <>
            <table width="100%">
                <thead>
                    <tr>
                        <th>Facility Name</th>
                        <th>Address</th>
                        <th>POC Firstname</th>
                        <th>POC Lastname</th>
                        <th>Mainline</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Timesheet approval </th>
                        <th>Scheduling</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                {(data!=undefined) ? (
                    <>
                        {data.map((val, index) =>
                            <tr key={index}>
                                <td>{val.facility_name}</td>
                                <td>{val.address}</td>
                                <td>{val.poc_firstname}</td>
                                <td>{val.poc_lastname}</td>
                                <td className='mobile-td'>{val.phone_no}</td>
                                <td className='email-td'>{val.email}</td>
                                
                                <td>
                                    <span className={val.is_active==true?`status-tag ${FaclityTrueStatusButtonClassName}`:`status-tag ${FaclityFalseStatusButtonClassName}`}>{val.is_active==true?"Active":"Deactive"}</span>
                                </td>
                                <td>
                                    <SwitchToggle dataSwitch={val.timesheet_approval} switchToggleID="timesheetApproveCheckbox" data={e=>handleTimesheetSwitch(e,val.facility_id)}/>

                                </td>
                                <td>
                                    <SwitchToggle dataSwitch={val.allow_scheduling} switchToggleID="schedulingCheckbox" data={e=>handleScheduleSwitch(e,val.facility_id)}/>

                                </td>
                                <td>
                                    <FacilityTableMenuDropdown data={val} handleRender={props.handleRender}/>
                                </td>
                            </tr>

                        )}
                    </>
                    ):""}
                </tbody>
            </table>

        </>
    )
}

export default UserFacilitiesTable;