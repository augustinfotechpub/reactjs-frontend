import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter} from "react-router-dom";
import App from './App';
import * as serviceWorker from './serviceWorker'
import { ContainerProvider } from './pages/utils/container'
import { ThemeProvider } from '@material-ui/styles'
import THEME from './pages/utils/theme'

const root = ReactDOM.createRoot(document.getElementById('root'));

console.clear()

root.render(
  <BrowserRouter>
  <ThemeProvider theme={THEME}>
    <ContainerProvider>
      <App />
    </ContainerProvider>
  </ThemeProvider>
  </BrowserRouter>
);
